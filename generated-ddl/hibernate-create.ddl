
    create table ancient_scroll (
       id char(36) default 'undefined' not null,
        history_version integer default 0 not null,
        length_of_scroll integer not null,
        primary key (id)
    );


    create table book (
       id char(36) default 'undefined' not null,
        history_version integer default 0 not null,
        editor varchar(255),
        friendly_id char(36) default 'undefined' not null,
        name varchar(50) default 'undefined',
        ordinal_position integer default 0 not null,
        page_count integer not null,
        time_zone varchar(50) not null,
        when_created timestamp default current timestamp not null,
        when_modified timestamp default current timestamp not null,
        who_created char(20) default 'undefined' not null,
        who_modified char(20) default 'undefined' not null,
        primary key (id)
    );


    create table chapter (
       id char(36) default 'undefined' not null,
        history_version integer default 0 not null,
        name varchar(50) default 'undefined',
        ordinal_position integer default 0 not null,
        time_zone varchar(50) not null,
        when_created timestamp default current timestamp not null,
        when_modified timestamp default current timestamp not null,
        who_created char(20) default 'undefined' not null,
        who_modified char(20) default 'undefined' not null,
        book_id char(36) default 'undefined',
        primary key (id)
    );


    create table crazy_long_web_page (
       id char(36) default 'undefined' not null,
        lines_per_scroll_pane integer not null,
        primary key (id)
    );


    create table encyclopedia_volume (
       name char(36) default 'undefined' not null,
        letter_of_alphabet char(36) default 'undefined' not null,
        primary key (name, letter_of_alphabet)
    );


    create table thesis (
       id char(36) default 'undefined' not null,
        history_version integer default 0 not null,
        time_zone varchar(255),
        when_created timestamp,
        when_modified timestamp,
        who_created varchar(255),
        who_modified varchar(255),
        primary key (id)
    );

create unique index UK_2rqqpgnve6gu3ar7prin5qm0i on book (friendly_id);


    alter table chapter 
       add constraint FKfxaijiug52tyrl5ifextmcfqb 
       foreign key (book_id) 
       references book;

