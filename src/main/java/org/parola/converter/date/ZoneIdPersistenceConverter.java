
package org.parola.converter.date;


import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.time.ZoneId;
import java.util.Objects;

/**
 * Converts {@link ZoneId} to {@link String} and back
 * in support of JPA persistence.
 * <p>
 * The existence of this class in the classpath and it being known by the persistence unit
 * is sufficient
 * to allow you to use the as-of Java SE 8 {@link ZoneId} class in
 * an {@link javax.persistence.Entity} or in other persistable classes.
 * <p>
 * Important: the setting of <code>@Converter(autoApply = true)</code>
 * in this class will make this conversion
 * automatically effective for all Entities that have one or more
 * persistent {@link ZoneId} properties.
 * <p>
 * The persistence provider must minimally support
 * <a href="https://jcp.org/aboutJava/communityprocess/final/jsr338/index.html">JPA 2.1</a>
 * for this to work.
 */
@Converter(autoApply = true)
public class ZoneIdPersistenceConverter implements AttributeConverter<ZoneId, String> {

    /**
     * @return Outputs this zone as a String, using the ID
     * @see ZoneId#toString()
     */
    @Override
    public String convertToDatabaseColumn(ZoneId entityValue) {
        return Objects.toString(entityValue, null);
    }


    @Override
    public ZoneId convertToEntityAttribute(String databaseValue) {
        if (databaseValue == null) {
            return null;
        }
        return ZoneId.of(databaseValue.trim());
    }

}
