package ch.ancora.id


import ch.ancora.audit.Identifiable
import org.apache.commons.lang3.RandomStringUtils
import java.util.*
import javax.persistence.PrePersist
import javax.persistence.PreUpdate

/**
 * Creates a generated Id on persistence lifecycle events.
 * Generates a short id of 7 characters. We have that here so that
 * we can compare various mixin behaviors
 *
 *
 * Created 2015-12-06.
 */
class GeneratedShortIdPersistenceListener {

    /**
     * automatic property set before any database persistence
     */
    @PreUpdate
    @PrePersist
    fun generateId(identifiableObject: Any) {
        if (identifiableObject is Identifiable) {
            when {
                identifiableObject.id.equals("") || identifiableObject.id.equals("undefined") -> {

                    val randomAlphabeticId = RandomStringUtils.randomAlphabetic(7)
                    val uuid = UUID.randomUUID().toString()
                    identifiableObject.id = randomAlphabeticId
                }
                else -> {
                   // println ("""do not need to generate an id ${identifiableObject.id}""")
                }
            }
            return
        }
        throw IllegalArgumentException("Class must implement " + Identifiable::class.java.canonicalName)
    }

}
