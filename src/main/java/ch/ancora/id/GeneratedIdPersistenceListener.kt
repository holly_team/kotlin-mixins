package ch.ancora.id


import ch.ancora.audit.Identifiable
import java.util.*
import javax.persistence.PrePersist
import javax.persistence.PreUpdate

/**
 * Creates a generated Id on persistence lifecycle events.
 *
 *
 * Created 2015-12-06.
 */
class GeneratedIdPersistenceListener {

    /**
     * automatic property set before any database persistence
     */
    @PreUpdate
    @PrePersist
    fun setId(identifiableObject: Any) {
        if (identifiableObject is Identifiable) {
            when {
                identifiableObject.id.equals("") || identifiableObject.id.equals("undefined") -> {
                    val uuid = UUID.randomUUID().toString()
                    identifiableObject.id = uuid
                }
                else -> {
                    // println ("""do not need to generate an id ${identifiableObject.id}""")
                }
            }
            return
        }
        throw IllegalArgumentException("Class must implement " + Identifiable::class.java.canonicalName)
    }

}
