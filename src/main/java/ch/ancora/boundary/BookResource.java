package ch.ancora.boundary;

import ch.ancora.controller.Bookshelf;
import ch.ancora.domain.Book;
import ch.ancora.domain.Chapter;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ResourceContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.UUID;


/**
 * The Book REST resource implementation.
 */
@Path("books")
@RequestScoped
public class BookResource {

//    @Inject
    private Bookshelf bookshelf;

    @Context
    private ResourceContext context;


    public BookResource() {
    }


    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Book books() {

        bookshelf = new Bookshelf();

        Book b = new Book();
        b.setId("testBook");

        b.setName("Go North Young Man");
        b.createAuditInfo();

        Chapter c = new Chapter();
        c.createAuditInfo();
        c.setId("c");

        b.getChapters().add(c);

        return b;
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/{id}")
    public Response get(@PathParam("id") String id) {

        System.out.println("TIGer");

        Book b = new Book();
        b.setId("testBook");
        b.setName("Go North Young Man");

        return Response.ok(b).build();
    }


    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/randomBookAsString")
    public String getHiGreeting() {
        Book book = new Book();
        book.setName("Random Thoughts of a Mislaid Tiger");
        book.createAuditInfo();
        book.setId(UUID.randomUUID().toString());
        return book.toString();
    }


}