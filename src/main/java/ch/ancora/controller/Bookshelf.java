package ch.ancora.controller;


import ch.ancora.domain.Book;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.Collections;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

//import lombok.AccessLevel;
//import lombok.AllArgsConstructor;
//import lombok.NoArgsConstructor;

/**
 * The Bookshelf implementation is used to find and managed books.
 */
@ApplicationScoped
@Transactional(Transactional.TxType.REQUIRED)
//@NoArgsConstructor(access = AccessLevel.PROTECTED)
//@AllArgsConstructor(access = AccessLevel.PROTECTED)
public class Bookshelf {


    @Inject
    private EntityManager entityManager;
    @Inject
    private Logger logger;

    public Bookshelf() {
    }

    Bookshelf(EntityManager em, Logger logger) {
        this.entityManager = em;
        this.logger = logger;
    }


    /**
     * Returns the list of all books in the shelf.
     *
     * @return a collection of books
     */
    public Collection<Book> findAll() {
        logger.log(Level.INFO, "Find all books.");
        TypedQuery<Book> findAll = entityManager.createNamedQuery(Book.FIND_ALL, Book.class);
        return Collections.unmodifiableCollection(findAll.getResultList());
    }

    /**
     * Check if book under given ISBN already exists.
     *
     * @param id the ISBN.
     * @return true of exists, otherwise false
     */
    public boolean exists(@NotNull String id) {
        try {
            logger.log(Level.INFO, "Find book with ID {0}.", id);
            final Book book = entityManager.find(Book.class, Objects.requireNonNull(id));
            return book != null;
        } catch (Exception e) {
            logger.log(Level.INFO, "Find book with ID {0} requires not-null value", id);
            return false;
        }
    }

    /**
     * Creates a new book in the bookshelf.
     *
     * @param book a book to create
     */
    public void create(Book book) {
        Objects.requireNonNull(book);
        logger.log(Level.INFO, "Creating {0}.", book);
        entityManager.persist(book);
    }


    /**
     * Creates a new book in the bookshelf.
     *
     * @param book a book to update
     */
    public void update(String id, Book book) {
        Objects.requireNonNull(book);
        logger.log(Level.INFO, "Updating {0} using ID {1}.", new Object[]{book, id});
        entityManager.merge(book);
    }

    /**
     * Deletes a book via ISBN.
     *
     * @param id the id
     */
    public void delete(String id) {
        Objects.requireNonNull(id);
        logger.log(Level.INFO, "Deleting book with ID {0}.", id);
        Book reference = entityManager.getReference(Book.class, id);
        entityManager.remove(reference);
    }

    /**
     * Find the book by its id and return a reference to it.
     *
     * @param id the id
     * @return the book
     */
    public Book findById(String id) {
        System.out.println("PUP");
        logger.log(Level.INFO, "Find book with ID {0}.", id);

        return entityManager.getReference(Book.class, Objects.requireNonNull(id));
    }


}
