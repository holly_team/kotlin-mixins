package ch.ancora.domain;

import ch.ancora.audit.Audit;
import ch.ancora.audit.Auditable;
import org.jetbrains.annotations.NotNull;

import javax.persistence.Entity;
import java.time.LocalDateTime;
import java.time.ZoneId;

/**
 * This illustrates a common alternative to mixins, the
 * Decorator pattern. Notice how much code we need!
 *
 *
 */

@Entity
public class Thesis extends Manuscript implements Auditable {

    private final Auditable audit = new Audit();

    @NotNull
    @Override
    public String getWhoCreated() {
        return audit.getWhoCreated();
    }

    @Override
    public void setWhoCreated(@NotNull String whoCreated) {
        audit.setWhoCreated(whoCreated);
    }

    @NotNull
    @Override
    public String getWhoModified() {
        return audit.getWhoModified();
    }

    @Override
    public void setWhoModified(@NotNull String newWhoModified) {
        audit.setWhoModified(newWhoModified);
    }

    @NotNull
    @Override
    public LocalDateTime getWhenCreated() {
        return audit.getWhenCreated();
    }

    @Override
    public void setWhenCreated(@NotNull LocalDateTime localDateTime) {
        audit.setWhenCreated(localDateTime);
    }

    @NotNull
    @Override
    public LocalDateTime getWhenModified() {
        return audit.getWhenModified();
    }

    @Override
    public void setWhenModified(@NotNull LocalDateTime localDateTime) {
        audit.setWhenModified(localDateTime);
    }

    @NotNull
    @Override
    public ZoneId getTimeZone() {
        return audit.getTimeZone();
    }

    @Override
    public void setTimeZone(@NotNull ZoneId zoneId) {
        audit.setTimeZone(zoneId);
    }

    @Override
    public void createAuditInfo() {
        setWhenCreated(LocalDateTime.now());
        setWhenModified(LocalDateTime.now());
        if (getWhoCreated().equals("")) {
            setWhoCreated("undefined");
        }
        if (getWhoModified().equals("")) {
            setWhoModified("undefined");
        }
        setTimeZone(ZoneId.systemDefault());
    }

    @Override
    public void updateAuditInfo() {
        setWhenModified(LocalDateTime.now());
    }
}
