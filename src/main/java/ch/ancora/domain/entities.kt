package ch.ancora.domain

import au.com.console.kassava.kotlinEquals
import au.com.console.kassava.kotlinToString
import ch.ancora.audit.*
import ch.ancora.id.GeneratedIdPersistenceListener
import ch.ancora.id.GeneratedShortIdPersistenceListener
import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonPropertyOrder
import java.util.*
import javax.persistence.*
import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlRootElement


/**
 * superclass handling identity concerns
 */
@MappedSuperclass
@EntityListeners(GeneratedIdPersistenceListener::class)
open class Manuscript : Identifiable,
        Versionable by Versioned() {

    @get:Id
    @get:Column(columnDefinition = "char(36) default 'undefined'")
    override var id: String = ""

}


/**
 * Demonstrates common approach of
 * use of a superclass for identity.
 */
@Entity
class AncientScroll : Manuscript() {

    var lengthOfScroll: Int = 0

}


/**
 * Mixed-in Identity and use of entity listener
 */
@Entity
@EntityListeners(GeneratedShortIdPersistenceListener::class)
class LongWebPage : Identifiable by Identifier() {

    var linesPerScrollPane: Int = 0


}

/**
 * Mixed-in identity with more behavior encapsulated in the implementation.
 * @see ShortIdentifier is an implementation of ShortIdentifiable
 */
@Entity
class VeryLongWebPage @JsonCreator constructor() : ShortIdentifiable by ShortIdentifier() {

    @get:JsonProperty
    var inchesPerScrollPane: Int = 0

}

/**
 * Mixed-in identity with more behavior encapsulated in the implementation.
 * This is a continuing test to see if the Json annotations are truly
 * required. Here they are omitted.
 *
 * @see ShortIdentifier entitShoryListener is in implementation of ShortIdentifiable
 */
@Entity
@JsonPropertyOrder(alphabetic = true) // order any properties that don't have explicit setting using alphabetic order
class VeryLongWebPageWithNoJsonAnnotations  :
        ShortIdentifiable by ShortIdentifier(),
        Nameable by Named() {

    //    @get:JsonProperty  appears to be extraneous: tests access this
    var inchesPerScrollPane: Int = 0

    //<editor-fold desc="hasCode, toString, equals implementations">
    // Here we optionally define properties for equals in a companion object.
    // In this way, Kotlin will generate fewer KProperty classes,
    // and we won't have array creation for every method call.
    companion object {
        private val equalsProperties = arrayOf(VeryLongWebPageWithNoJsonAnnotations::id)
        private val toStringAndCopyProperties = arrayOf(VeryLongWebPageWithNoJsonAnnotations::id,
                VeryLongWebPageWithNoJsonAnnotations::inchesPerScrollPane,
                VeryLongWebPageWithNoJsonAnnotations::name)
    }

    // id does not necessarily exist at creation, so use friendlyId
    override fun equals(other: Any?) = kotlinEquals(other = other, properties = equalsProperties)

    override fun hashCode(): Int {
        return Objects.hash(id)
    }

    override fun toString() = kotlinToString(properties = toStringAndCopyProperties)
    //</editor-fold>

}


/**
 * Demonstration that complex keys can be outsourced to mixins
 */
@Entity
@IdClass(CompositeIdentifiable::class)
class EncyclopediaVolume :
        CompositeIdentifiable by CompositeId() {

    //<editor-fold desc="hasCode, toString, equals implementations">
    // Here we optionally define properties for equals in a companion object.
    // In this way, Kotlin will generate fewer KProperty classes,
    // and we won't have array creation for every method call.
    companion object {
        private val equalsProperties = arrayOf(EncyclopediaVolume::name)
        private val toStringAndCopyProperties = arrayOf(EncyclopediaVolume::name,
                EncyclopediaVolume::letterOfAlphabet)
    }

    // id does not necessarily exist at creation, so use friendlyId
    override fun equals(other: Any?) = kotlinEquals(other = other, properties = equalsProperties)


    override fun toString() = kotlinToString(properties = toStringAndCopyProperties)

    override fun hashCode(): Int {
        return Objects.hash(name, letterOfAlphabet)
    }
    //</editor-fold>

}


/**
 * Demonstration of how we can mixin all kinds of things,
 * making our class more readable and the mixin functionality more
 * reusable. This is effectively a type of composition.
 *
 * What is of interest in this class is the fact
 * that it inherits state from multiple sources, that is
 * inheritance via delegation, a form of mixin.
 *
 *
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.PROPERTY)
@Entity
@NamedQuery(name = Book.FIND_ALL, query = "SELECT b FROM Book b")
class Book : Manuscript(),
        FriendlyIdentifiable by FriendlyId(),
        Nameable by Named(),
        Auditable by Audit(),
        Orderable by Ordered() {


    var editor: String = ""

    @get:OneToMany(mappedBy = "book", cascade = [CascadeType.PERSIST])
    var chapters: MutableList<Chapter> = ArrayList()

    var pageCount: Int = 0

    //<editor-fold desc="hasCode, toString, equals implementations">
    // Here we optionally define properties for equals in a companion object.
    // In this way, Kotlin will generate fewer KProperty classes,
    // and we won't have array creation for every method call.
    companion object {
        private val equalsProperties = arrayOf(Book::friendlyId)
        private val toStringAndCopyProperties = arrayOf(Book::friendlyId,
                Book::id, Book::name,
                Book::ordinalPosition)

        const val FIND_ALL = "Book.findAll"
    }

    // id does not necessarily exist at creation, so use friendlyId
    override fun equals(other: Any?) = kotlinEquals(other = other, properties = equalsProperties)

    /**
     * friendlyId is always unique and is the only thing we need in hashCode.
     */
    override fun hashCode(): Int {
        return Objects.hash(friendlyId)
    }

    override fun toString() = kotlinToString(properties = toStringAndCopyProperties)
    //</editor-fold>

}


/**
 * This class demonstrates support for JPA versioning
 * via mixins and how nothing is stopping us from providing in-class
 * implementations of interfaces.
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.PROPERTY)
@Entity
class Chapter :
        Identifiable,
        Auditable by Audit(),
        Orderable by Ordered(),
        Nameable by Named(),
        Versionable by Versioned() {


    // here we did not want to outsource our Id implementation as we wanted an Id
    // that was really long.
    @get:Id
    @get:Column(columnDefinition = "char(72) default 'undefined'")
    override var id: String = ""
        get() {
            if (field == null || field.equals("") || field.equals("undefiend")) {
                field = UUID.randomUUID().toString() + UUID.randomUUID().toString()
            }
            return field
        }


    @get:ManyToOne(cascade = [CascadeType.ALL])
    var book: Book? = null


    //<editor-fold desc="hasCode, toString, equals implementations">

    companion object {
        private val equalsProperties = arrayOf(Chapter::id)
        private val toStringProperties = arrayOf(Chapter::id, Chapter::name)
    }

    override fun equals(other: Any?) = kotlinEquals(other = other, properties = equalsProperties)

    override fun hashCode(): Int {
        return Objects.hash(Chapter::id)
    }

    override fun toString() = kotlinToString(properties = toStringProperties)
    //</editor-fold>


}
