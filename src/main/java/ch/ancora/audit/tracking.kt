package ch.ancora.audit

import org.apache.commons.lang3.RandomStringUtils
import org.hibernate.annotations.NaturalId
import java.io.Serializable
import java.time.LocalDateTime
import java.time.ZoneId
import javax.persistence.*


/**
 * Information we need to know who created or modified an entity and when.
 */
interface Auditable : Serializable {

    @get:Column(columnDefinition = "char(20) default 'undefined' not null")
    var whoCreated: String

    @get:Column(columnDefinition = "char(20) default 'undefined' not null")
    var whoModified: String

    @get:Column(columnDefinition = "timestamp default current timestamp not null")
    var whenCreated: LocalDateTime

    @get:Column(columnDefinition = "timestamp default current timestamp not null")
    var whenModified: LocalDateTime

    @get:Column(columnDefinition = "varchar(50) not null")
    var timeZone: ZoneId

    @PrePersist
    fun createAuditInfo()

    @PreUpdate
    fun updateAuditInfo()
}

/**
 * Fields we need for auditable
 */
class Audit : Auditable {
    override var whoCreated: String = "undefined"
    override var whoModified: String = "undefined"
    override var whenCreated: LocalDateTime = LocalDateTime.now()
    override var whenModified: LocalDateTime = LocalDateTime.now()

    override var timeZone: ZoneId = ZoneId.systemDefault()

    override fun createAuditInfo() {
        whenCreated = LocalDateTime.now()
        whenModified = LocalDateTime.now()
        if (whoCreated == "") {
            whoCreated = "undefined"
        }
        if (whoModified == "") {
            this.whoModified = "undefined"
        }
        this.timeZone = ZoneId.systemDefault()
    }

    override fun updateAuditInfo() {
        this.whenModified = LocalDateTime.now()
    }

}

/**
 * Lots of things get names.
 */
interface Nameable : Serializable {

    @get:Column(columnDefinition = "varchar(50) default 'undefined'")
    var name: String
}

/**
 * Created 2015-12-03.
 */
//@Embeddable
class Named : Nameable {

    override var name: String = "undefined"

}


/**
 * Used for when order is important
 */
interface Orderable : Serializable {

    @get:Column(columnDefinition = "integer default 0 not null")
    var ordinalPosition: Int
}


/**
 */
class Ordered : Orderable {

    override var ordinalPosition: Int = 0

}


/**
 * A version
 */
interface Versionable : Serializable {

    @get:Version
    @get:Column(columnDefinition = "integer default 0 not null")
    var historyVersion: Int   // the word version results in the columnDefinition not kicking in

}


class Versioned : Versionable {

    override var historyVersion: Int = 0


}


/**
 * The purpose of this class is to enable checks and operations
 * in various listeners.
 */
interface Identifiable {

    @get:Id
    @get:Column(columnDefinition = "char(36) default 'undefined'")
    var id: String

}

class Identifier : Identifiable {
    override var id: String = ""
}

/**
 * The purpose of this class is to enable checks and operations
 * in various listeners.
 */
interface ShortIdentifiable {


    @get:Id
    @get:Column(columnDefinition = "char(7) default 'undefined'")
    var id: String

    /**
     * automatic property set before any database persistence
     */
    @PreUpdate
    @PrePersist
    fun generateId()

}

class ShortIdentifier : ShortIdentifiable {

    override var id: String = ""

    /**
     * automatic property set before any database persistence
     */
    override fun generateId() {
        if (id == null
                || id.equals("")
                || id.equals("undefined")) {

            println(" need to generate a short id")
            val randomAlphabeticId = RandomStringUtils.randomAlphabetic(4)
            id = randomAlphabeticId
        }
    }
}

interface CompositeIdentifiable : Serializable {

    @get:Id
    @get:Column(columnDefinition = "char(36) default 'undefined'")
    var name: String
    @get:Id
    @get:Column(columnDefinition = "char(36) default 'A'")
    var letterOfAlphabet: String
}

class CompositeId : CompositeIdentifiable {

    override var name: String = ""
    override var letterOfAlphabet: String = ""

}

/**
 * Also known as a business or a surrogate key
 */
interface FriendlyIdentifiable {

    @get:NaturalId
    @get:Column(columnDefinition = "char(36) default 'undefined' not null")
    var friendlyId: String

}

class FriendlyId(override var friendlyId: String = "") : FriendlyIdentifiable

//    override var friendlyId: String = ""
//}


