package persistence;

import org.apache.commons.io.FileUtils;
import org.hibernate.engine.jdbc.internal.DDLFormatterImpl;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Create a file to store the DDL, deleting it first if it exists.
 * JPA 2 can also create such a file, but it is not formatted.
 * This is a convenience class that is used by explicit DDL creators.
 *
 */
class DDLFileCreator {

    private static final String LINE_SEPARATOR = System.getProperty("line.separator");
    private static final String FILE_SEPARATOR = System.getProperty("file.separator");


    /**
     * First deletes file if exists and then writes the DDL to it.
     *
     * @param ddlStatements the statements to generate
     * @param unqualifiedFileName a file name
     */
    public static void createDDLFile(String ddlStatements, String unqualifiedFileName) {

        String workingDir = System.getProperty("user.dir");

        String fileName = workingDir + FILE_SEPARATOR + "generated-ddl"  + FILE_SEPARATOR +  unqualifiedFileName;
        Path path = Paths.get(fileName);
        try {
            Files.deleteIfExists(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        File file = new File(fileName);
        try {
            FileUtils.writeStringToFile(file, "-- DDL For Mixins", java.nio.charset.Charset.forName("UTF-8"));
            FileUtils.writeStringToFile(file, LINE_SEPARATOR, java.nio.charset.Charset.forName("UTF-8"), true);
            FileUtils.writeStringToFile(file, ddlStatements,  java.nio.charset.Charset.forName("UTF-8"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static String prettifyDDLOutput(String unformatted) {
        StringReader lowLevel = new StringReader(unformatted);
        BufferedReader highLevel = new BufferedReader(lowLevel);
        DDLFormatterImpl formatter = new DDLFormatterImpl();

        StringBuilder sb = new StringBuilder();
        highLevel.lines().forEach(x -> {
            String formattedSqlStatement = formatter.format(x + ";");
            sb.append(formattedSqlStatement);
            sb.append(System.getProperty("line.separator"));
            sb.append(System.getProperty("line.separator"));
        });

        return sb.toString();
    }

}


