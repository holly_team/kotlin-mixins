package persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * @author S Gertiser, created 2018-11-03.
 */
public class DropTableUtility {

    public static void dropTable(String schema, String table) {
        try {
            Connection conn = DriverManager.getConnection(
                    "jdbc:derby:memory:mixin");
            PreparedStatement ps = conn.prepareStatement
                    ("drop table " + schema + "." + table);
            ps.execute();
            ps.close();
        } catch (SQLException ignored) {
        }
    }


}
