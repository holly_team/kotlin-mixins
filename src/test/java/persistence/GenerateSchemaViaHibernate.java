package persistence;

import javax.persistence.Persistence;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * Create DDL using Hibernate as the persistence provider.
 *
 * @author S Gertiser
 */
class GenerateSchemaViaHibernate {

    @SuppressWarnings("unchecked")
    public static void main(String[] args) {
        Map properties = new HashMap<String, Object>();
        properties.put("javax.persistence.provider", "org.hibernate.jpa.HibernatePersistenceProvider");
        properties.put("javax.persistence.schema-generation.scripts.action", "create");
        StringWriter stringWriter = new StringWriter();
        properties.put("javax.persistence.schema-generation.scripts.create-target", stringWriter);
        Persistence.generateSchema("mixin-unit-hibernate", properties);


        String prettifiedDDLOutput = DDLFileCreator.prettifyDDLOutput(stringWriter.toString());

        String fileName = "hibernate-create.ddl";
        DDLFileCreator.createDDLFile(prettifiedDDLOutput, fileName);

        System.exit(0);

    }

}
