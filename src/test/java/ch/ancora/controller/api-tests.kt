package ch.ancora.controller

import ch.ancora.domain.Book
import org.hamcrest.MatcherAssert.assertThat
import org.junit.jupiter.api.Test
import java.time.LocalDateTime
import org.hamcrest.CoreMatchers.`is` as Is

/**
 * This tests that the basic mixin premise works
 *
 */
class MixinTest {

    @Test
    fun `ensure that we can access the audit fields`() {
        val book = Book()
        book.id = "some id"
        book.whoCreated = "sally"
        book.whenCreated = LocalDateTime.now()

        // this field is mixed in. the assertion succeeds because it finds it.
        assertThat(book.whoModified, Is("undefined"))
        assertThat(book.historyVersion, Is(0))
        // .. that should do it, further mixin verifications superfluous
    }




}