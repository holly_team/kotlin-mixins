package ch.ancora.controller

import base.AbstractPersistence
import base.UnitPersistence
import ch.ancora.domain.Book
import org.hamcrest.CoreMatchers
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.greaterThan
import org.hamcrest.Matchers.hasSize
import org.junit.Assert
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.util.logging.Logger
import org.hamcrest.CoreMatchers.`is` as Is


/**
 * Test the bookshelf controller
 */
internal class BookshelfTest : UnitPersistence() {

    private var bookshelf: Bookshelf? = null


    @BeforeEach
    fun setUp() {
        bookshelf = Bookshelf(AbstractPersistence.entityManager, Logger.getAnonymousLogger())
    }

    @Test
    fun findAll() {
        val books = bookshelf!!.findAll()
        // we have 4 at create time via script
        assertThat(books, hasSize(greaterThan(3)))
    }

    @AfterEach
    fun tearDown() {
    }

    @Test
    fun `test that we can determine whether a book exists`() {
        // loaded via script, should exist
        var exists = bookshelf!!.exists("1788393856")
        assertThat(exists, Is(true))

        exists = bookshelf!!.exists("aKnownUnknownId")
        assertThat(exists, Is(false))

        exists = bookshelf!!.exists(null)
        assertThat(exists, Is(false))
    }

    @Test
    fun `ensure that we can create a book`() {
        // we are not setting the id, Hibernate is doing it for us.
        val book = Book()
        println(book.whenCreated)
        book.friendlyId = "WH"
        book.name = "Go North Young Man"
        book.id = "123489384"

        try {
            bookshelf!!.create(book)
        } catch (e: Exception) {
            Assert.fail("No exception was expected.")
        }
    }

    @Test
    fun update() {
    }

    @Test
    fun delete() {
    }

    @Test
    fun findById() {
    }
}
