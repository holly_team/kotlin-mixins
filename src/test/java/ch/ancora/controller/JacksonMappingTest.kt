package ch.ancora.controller

import ch.ancora.audit.*
import ch.ancora.domain.VeryLongWebPageWithNoJsonAnnotations
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import org.hamcrest.CoreMatchers.containsString
import org.hamcrest.MatcherAssert.assertThat
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import java.io.IOException
import java.time.LocalDateTime
import java.time.ZoneId
import org.hamcrest.CoreMatchers.`is` as Is


/**
 * Various Proofs that Jackson serialization works with mixed in classes
 *
 */
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class JacksonMappingTest {

    var objectMapper: ObjectMapper = ObjectMapper()

    @BeforeAll
    fun init() {
        objectMapper.registerModule(JavaTimeModule())
        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
    }


    /**
     * This is a sanity check to prove that mixed in classes are serialized by Jackson
     */
    @Test
    @Throws(IOException::class)
    fun `ensure that we can convert Json to Java`() {

        val carJson = """
               {
                    "brand":"VW","doors":4,
                    "name":"Beetle",
                    "id":"7654321",
                    "whoCreated":"Sally",
                    "whenCreated":"2018-10-18T13:02:36.314",
                    "whoModified":"Benny",
                    "whenModified":"2018-11-18T13:02:36.322",
                    "timeZone":"Europe/Zurich"
                }
            """

        val car = objectMapper.readValue(carJson, Car::class.java)

        assertThat(car.id, Is("7654321"))
        assertThat(car.doors, Is(4))

        // these are all mixed in
        assertThat(car.name, Is("Beetle"))
        assertThat(car.whoCreated, Is("Sally"))
        assertThat(car.whoModified, Is("Benny"))
        assertThat(car.whenCreated, Is(LocalDateTime.parse("2018-10-18T13:02:36.314")))
        assertThat(car.timeZone, Is(ZoneId.of("Europe/Zurich")))

    }


    /**
     * A sanity check to ensure that our mixed in classes are recognized by a popular library
     */
    @Test
    @Throws(IOException::class)
    fun `ensure that we can convert Java to Json`() {
        val car = Car()
        car.brand = "VW"
        car.doors = 4

        // mixed-in values
        car.id = "123457"
        car.name = "Beetle"
        car.whoCreated = "Joseph Ganz"
        car.whoModified = "Porsche"
        car.whenCreated = LocalDateTime.parse("1933-07-15T13:02:36.31")
        car.whenModified = LocalDateTime.parse("1935-08-16T13:02:36.31")
        car.timeZone = ZoneId.of("Pacific/Samoa")


        val carJson = objectMapper.writeValueAsString(car)

        println("carJson = ${carJson}")


        assertThat(carJson, containsString("VW"))
        assertThat(carJson, containsString("Beetle"))
        assertThat(carJson, containsString("\"doors\":4,"))

        // mixed in assertions
        assertThat(carJson, containsString("123457"))
        assertThat(carJson, containsString("\"whoCreated\":\"Joseph Ganz\""))
        assertThat(carJson, containsString("\"whoModified\":\"Porsche\""))
        assertThat(carJson, containsString("\"whenCreated\":\"1933-07-15T13:02:36.31\""))
        assertThat(carJson, containsString("whenModified\":\"1935-08-16T13:02:36.31"))
        assertThat(carJson, containsString("\"timeZone\":\"Pacific/Samoa\""))

    }

    @Test
    @Throws(IOException::class)
    internal fun `test if deserialize of mixins respects json order annotation as a sanity check`() {
        val three = VeryLongWebPageWithNoJsonAnnotations()
        three.id = "5678901"
        three.inchesPerScrollPane = 7
        three.name = "Jimminy Cricket"

        val returnedValue = ObjectMapper().writeValueAsString(three)

        // platform independent test. quotation marks in Strings fail junit equality checks in a
        // cross-platform manner, so we swap quotes out with an @ sign.
        var expectedValue = """{"id":"5678901","inchesPerScrollPane":7,"name":"Jimminy Cricket"}""".replace("\"".toRegex(), "@")

        assertThat(returnedValue.replace("\"".toRegex(), "@"), containsString( expectedValue))


    }                                                                         
}


internal class Car : Identifiable by Identifier(), Nameable by Named(), Auditable by Audit() {

    var brand: String? = null
    var doors = 0
    override fun toString(): String {
        return "Car(brand=$brand, doors=$doors), name=$name, whoCreated=$whoCreated, whenCreated=$whenCreated, " +
                "whoModified=$whoModified, whenModified=$whenModified, timeZone=$timeZone"
    }


}