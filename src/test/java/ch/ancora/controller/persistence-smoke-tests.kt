package ch.ancora.controller

import base.UnitPersistence
import ch.ancora.domain.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.hamcrest.CoreMatchers
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.number.OrderingComparison.greaterThan
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import javax.persistence.EntityExistsException
import org.hamcrest.CoreMatchers.`is` as Is

/**
 * Various tests of our mixin classes in the context of persistence
 *
 *
 */


class UnitPersistenceTest : UnitPersistence() {


    /*
     * Sanity check
     */
    @Test
    fun `ensure that can fetch what we persisted`() {
        // we are not setting the id, Hibernate is doing it for us.
        val book = Book()
        println(book.whenCreated)
        book.friendlyId = "WH"
        book.name = "Wuthering Heights"
        book.id = "1234"

        createEntityManager()
        entityManager.transaction.begin()
        entityManager.persist(book)

        entityManager.transaction.commit()
        val foundBook = entityManager.find(Book::class.java, book.id)
        assertThat(foundBook, Is(CoreMatchers.notNullValue()))
        assertThat(foundBook.id, Is(book.id))
    }

    /*
     *  the following test will compare the whenCreated value to see if it changed.
     *  the value after persisting should be later than the value before persisting.
     *  this proves that JPA is respecting a mixed-in value, here whenCreated.
     */
    @Test
    fun `ensure that mix-ins are respected by JPA`() {
        val book = Book()
        val beforePersistenceCreatedOnValue = book.whenCreated
        book.friendlyId = "David Copperfield"
        book.id = "david"
        book.name = "David Copperfield"

        GlobalScope.launch {
            // wait a bit so that the persistence manager can
            // get a different audit value than the one provided at class creation.
            delay(1000)
            createEntityManager()
            entityManager.transaction.begin()
            entityManager.persist(book)
            entityManager.transaction.commit()
            val foundBook = entityManager.find(Book::class.java, book.id)
            println(foundBook.hashCode())

            val afterPersistenceCreatedOnValue = foundBook.whenCreated
            assertThat(afterPersistenceCreatedOnValue, greaterThan(beforePersistenceCreatedOnValue))
        }
    }


    @Test
    fun `ensure collections can be persisted and that mixins also work in these`() {
        // we are not setting the id, Hibernate is doing it for us.
        val book = Book()
        book.friendlyId = "Arabian Nights"
        book.id = "arabian"
        book.name = "Arabian Nights"
        book.editor = "unknown"

        val chapter1 = Chapter()
        chapter1.book = book
        chapter1.id = "arabian-scheherazade"
        chapter1.ordinalPosition = 1
        book.chapters.add(chapter1)
        chapter1.name = "One Thousand and One Nights"

        createEntityManager()
        entityManager.transaction.begin()
        entityManager.persist(book)
        entityManager.transaction.commit()

        val foundChapter = entityManager.find(Chapter::class.java, chapter1.id)
        assertThat(foundChapter.name, Is("One Thousand and One Nights"))

    }

    @Test
    fun `ensure that the id population works`() {
        val book = Book()
        book.name = "Grimm's Fairy Tales"

        createEntityManager()
        entityManager.transaction.begin()
        entityManager.persist(book)
        entityManager.transaction.commit()

        assertThat(book.id.length, Is(36))

    }


    @Test
    fun `test that JPA versioning is respected`() {
        val scroll = AncientScroll()
        scroll.lengthOfScroll = 34
        createEntityManager()
        entityManager.transaction.begin()
        entityManager.persist(scroll)
        entityManager.transaction.commit()

        val foundScroll = entityManager.find(AncientScroll::class.java, scroll.id)
        assertThat(foundScroll.lengthOfScroll, Is(34))
        assertThat(foundScroll.historyVersion, Is(0))

        // perform an update
        foundScroll.lengthOfScroll = 1012
        entityManager.transaction.begin()
        entityManager.persist(foundScroll)
        entityManager.transaction.commit()
        val foundScroll2 = entityManager.find(AncientScroll::class.java, scroll.id)

        assertThat(foundScroll2.lengthOfScroll, Is(1012))
        assertThat(foundScroll2.historyVersion, Is(1))   // version updated
    }

    /**
     * In normal JPA, it is not possible to put Id into an embeddable class without
     * having to repeat all the information in the @Entity class as well.
     * But here, we prove that we can!
     *
     */
    @Test
    fun `test that we can outsource @Id to a mix-in`() {
        val crazyLongWebPage = LongWebPage()
        crazyLongWebPage.linesPerScrollPane = 20

        assertThat(crazyLongWebPage.id, Is(""))

        createEntityManager()
        entityManager.transaction.begin()
        entityManager.persist(crazyLongWebPage)
        entityManager.transaction.commit()

        val foundWebPage = entityManager.find(LongWebPage::class.java, crazyLongWebPage.id)
        assertThat(foundWebPage.id, Is(notNullValue()))
        assertThat(foundWebPage.id.length, greaterThan(0))
    }


    /*
     * The longWebPage uses a different Id listener than all of the
     * other classes.
     */
    @Test
    fun `prove that listeners can be swapped out`() {
        val longWebPage = LongWebPage()
        longWebPage.linesPerScrollPane = 90

        createEntityManager()
        entityManager.transaction.begin()
        entityManager.persist(longWebPage)
        entityManager.transaction.commit()

        val foundWebPage = UnitPersistence.entityManager.find(LongWebPage::class.java, longWebPage.id)
        assertThat(foundWebPage.id.length, Is(7))
    }

    /**
     * We are testing that we can outsource a composite key to a mix-in.
     */
    @Test
    fun `prove that we can outsource composite keys to a mixin`() {
        val encyclopediaVolume1 = EncyclopediaVolume()
        encyclopediaVolume1.name = "Britannica"
        encyclopediaVolume1.letterOfAlphabet = "A"

        val encyclopediaVolume2 = EncyclopediaVolume()
        encyclopediaVolume2.name = "Britannica"
        encyclopediaVolume2.letterOfAlphabet = "B"

        val encyclopediaVolume3 = EncyclopediaVolume()
        encyclopediaVolume3.name = "Britannica"
        encyclopediaVolume3.letterOfAlphabet = "B"

        createEntityManager()
        entityManager.transaction.begin()
        entityManager.persist(encyclopediaVolume1)
        entityManager.transaction.commit()

        entityManager.transaction.begin()
        entityManager.persist(encyclopediaVolume2)
        entityManager.transaction.commit()

        assertThrows<EntityExistsException> {
            entityManager.transaction.begin()
            entityManager.persist(encyclopediaVolume3)
            entityManager.transaction.commit()
        }

    }

    /**
     * The veryLongWebPage does not use a persistence lifecycle
     * listener. Instead the persistence listeners could be declared
     * by a mixed in interface with corresponding implementation.
     */
    @Test
    fun `prove that listeners can be mixed in`() {
        val veryLongWebPage = VeryLongWebPage()
        veryLongWebPage.inchesPerScrollPane = 7

        createEntityManager()
        entityManager.transaction.begin()
        entityManager.persist(veryLongWebPage)
        entityManager.transaction.commit()

        val foundWebPage = UnitPersistence.entityManager.find(VeryLongWebPage::class.java, veryLongWebPage.id)
        assertThat(foundWebPage.id.length, Is(4))
    }

}