package ch.ancora.boundary

import ch.ancora.controller.Bookshelf
import ch.ancora.domain.Book
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import org.glassfish.jersey.server.ResourceConfig
import org.glassfish.jersey.test.JerseyTest
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

import javax.ws.rs.Consumes
import javax.ws.rs.Produces
import javax.ws.rs.container.ContainerRequestContext
import javax.ws.rs.container.ContainerRequestFilter
import javax.ws.rs.core.Application
import javax.ws.rs.core.Context
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response
import javax.ws.rs.ext.ContextResolver
import javax.ws.rs.ext.Provider
import javax.ws.rs.ext.Providers
import java.io.IOException

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.*
import org.hamcrest.core.Is
import org.hamcrest.Matchers.`is` as Is


/**
 * A set of sanity checks to see that mixed in annotations
 * work with REST
 */
internal class BookResourceIntegrationTest : JerseyTest() {


    override fun configure(): Application {

        val config = ResourceConfig()
        config.register(BookResource::class.java)
//        config.register(Bookshelf::class.java)
//        config.register(OMContextResolver::class.java)
//        config.register(Filter::class.java)
        return config
    }


//    public override fun configure(): Application {
//        val config = ResourceConfig()
//        config.register(TestResource::class.java)
//        config.register(OMContextResolver::class.java)
//        config.register(Filter::class.java)
//        return config
//    }


    @BeforeEach
    @Throws(Exception::class)
    fun setUpChild() {
        super.setUp()             // important. JerseyTest does stuff.

    }

    @AfterEach
    @Throws(Exception::class)
    fun tearDownChild() {
        super.tearDown()
    }

    @Test
    fun `ensure I can get a book as a string`() {
        val response = target("books/randomBookAsString").request().get()
        assertThat(response.status, Is(Response.Status.OK.statusCode))

        val readEntity = response.readEntity(String::class.java)
        assertThat(readEntity, containsString("Random Thoughts of a Mislaid Tiger"))
    }

    @Test
    internal fun `ensure that I can retrieve all books`() {

        val response = target("books").request().get()

        val book = response.readEntity(Book::class.java)

        println("book = ${book}")


        assertThat(true, Is(true))

    }

    @Test
    @DisplayName("ensure I can get a book by Id via REST")
    fun ensureThatBookRetrievalByIdWorks() {
        val response = target("books/0345391802").request().get()
        println("response = $response")
        val book = response.readEntity(Book::class.java)

        println("book = $book")


    }


}