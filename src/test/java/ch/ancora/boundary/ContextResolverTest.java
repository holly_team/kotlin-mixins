package ch.ancora.boundary;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Assert;
import org.junit.Test;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Response;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;


/**
 * Test that we can get a content resolver so that we can use it
 * elsewhere in application specific tests.
 */
public class ContextResolverTest extends JerseyTest {

    @Path("test")
    public static class TestResource {
        @GET
        public String dummyGet() {
            return "Boo";
        }
    }

    @Override
    public Application configure() {
        ResourceConfig config = new ResourceConfig();
        config.register(TestResource.class);
        config.register(OMContextResolver.class);
        config.register(Filter.class);
        return config;
    }

    @Test
    public void contextResolverIsOk() {
        Response response = target("test").request().get();
        assertThat(response.getStatus(), is(200));
        Assert.assertEquals(200, response.getStatus());
        assertThat(response.readEntity(String.class), is("resolver found"));
        response.close();
    }
}

