package ch.ancora.boundary;

import com.fasterxml.jackson.databind.ObjectMapper;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;
import javax.ws.rs.ext.Providers;
import java.io.IOException;

/**
 */
@Provider
public class Filter implements ContainerRequestFilter {

    @Context
    private Providers providers;

    @Override
    public void filter(ContainerRequestContext requestContext) {
        ContextResolver<ObjectMapper> contextResolver
                = providers.getContextResolver(ObjectMapper.class,
                MediaType.APPLICATION_JSON_TYPE);
        if (contextResolver == null) {
            requestContext.abortWith(
                    Response.serverError().entity("no resolver").build());
        } else {
            ObjectMapper mapper = contextResolver.getContext(null);
            if (mapper == null) {
                requestContext.abortWith(
                        Response.serverError().entity("no mapper").build());
                return;
            }
            requestContext.abortWith(
                    Response.ok("resolver found").build());
        }
    }
}
