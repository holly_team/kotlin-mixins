package base;


import org.junit.jupiter.api.BeforeAll;

import javax.persistence.Persistence;

/**
 * Abstract class for unit persistence tests
 */
public abstract class UnitPersistence extends AbstractPersistence {

    @BeforeAll
    public static void createEntityManagerFactory() {
        emf = Persistence.createEntityManagerFactory("mixin-unit");
    }


}
