package base;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Abstract class for persistence tests
 */
public abstract class AbstractPersistence {

    static EntityManagerFactory emf;
    public static EntityManager entityManager;


    @BeforeEach
    public void createEntityManager() {
        entityManager = emf.createEntityManager();
    }

    public static EntityManager getEntityManager() {
        return entityManager;
    }

    protected Object persist(Object object) {
        return persist(object, entityManager);
    }

    private Object persist(Object object, EntityManager em) {

        if (!em.getTransaction().isActive()) {
            em.getTransaction().begin();
        }
        em.persist(object);
        em.flush();
        em.refresh(object);
        em.getTransaction().commit();
        System.out.println("after commit");
        return object;
    }

    protected void remove(Object object) {
        remove(object, entityManager);
    }

    private void remove(Object object, EntityManager em) {
        // the following check is required since we are creating the database
        // from scratch every time and it apparently is leaving things uncommitted.
        if (!em.getTransaction().isActive()) {
            em.getTransaction().begin();
        }
        em.remove(object);
        em.getTransaction().commit();
    }

    /**
     * Get metadata to see that the mapping worked.
     *
     * @return The SQL datatype
     */
    public String getSQLColumnType(String tableName, String columnName) throws SQLException {
        entityManager.getTransaction().begin();
        final Connection connection = entityManager.unwrap(Connection.class);
        final DatabaseMetaData databaseMetaData = connection.getMetaData();
        final String catalog = null;
        final String schemaPattern = "TEST";
        ResultSet resultSet = databaseMetaData.getColumns(
                catalog, schemaPattern, tableName, columnName);
        String actualType = null;
        while (resultSet.next()) {
            actualType = resultSet.getString("TYPE_NAME");
        }
        return actualType;
    }


    @AfterEach
    public void closeConnection() {
        if (entityManager != null) {
            if (entityManager.isOpen()) {
                if (entityManager.getTransaction().isActive()) {
                    entityManager.getTransaction().commit();
                }
                entityManager.close();
            }
        }
    }

    @AfterAll
    public static void closeTestFixture() {
        if (emf != null) {
            if (emf.isOpen()) {
                emf.close();
            }
        }
    }

}
