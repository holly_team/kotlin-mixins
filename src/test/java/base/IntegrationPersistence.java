package base;

import org.junit.jupiter.api.BeforeAll;

import javax.persistence.Persistence;

/**
 * Abstract class for integration persistence tests
 */
class IntegrationPersistence extends AbstractPersistence {

    @BeforeAll
    public static void createEntityManagerFactory() {
        emf = Persistence.createEntityManagerFactory("mixin-integration");
    }
}
