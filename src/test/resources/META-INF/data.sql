

INSERT INTO book (id, name, author) VALUES ('0345391802', 'The Hitchhiker''s Guide to the Galaxy', 'Douglas Adams');
INSERT INTO book (id, name, author) VALUES ('1788393856', 'Architecting Modern Java EE Applications', 'Sebastian Daschner');
INSERT INTO book (id, name, author) VALUES ('1788293673', 'Java EE 8 Application Development', 'David R. Heffelfinger');
INSERT INTO book (id, name, author) VALUES ('1786469200', 'Mastering Java EE 8 Application Development', 'Kapila Bogahapitiya,  Sandeep Nair');
