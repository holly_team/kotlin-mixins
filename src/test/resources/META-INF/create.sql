CREATE TABLE APP.Book (id CHAR(36) DEFAULT 'undefined' PRIMARY KEY NOT NULL,  name VARCHAR(50) default 'undefined',  friendly_id varchar(50) default 'undefined',  author varchar(50) default 'undefined', editor varchar(50) default 'undefined', ordinal_position INT DEFAULT 0,  page_count int default 0, history_version INT DEFAULT 0,  when_created TIMESTAMP DEFAULT current timestamp,  when_modified TIMESTAMP DEFAULT current timestamp,  who_created VARCHAR(15) DEFAULT 'undefined',  who_modified VARCHAR(15) DEFAULT 'undefined', time_zone VARCHAR(30) default 'Europe/Andorra');
CREATE UNIQUE INDEX Book_id_uindex ON APP.Book (id);
CREATE UNIQUE INDEX Book_name_uindex ON APP.Book (name);
create table app.chapter (id CHAR(72) DEFAULT 'undefined' PRIMARY KEY NOT NULL,  name VARCHAR(50) ,  friendly_id varchar(50),  ordinal_position INT DEFAULT 0,  history_version INT DEFAULT 0,  when_created TIMESTAMP DEFAULT current timestamp,  when_modified TIMESTAMP DEFAULT current timestamp,  who_created VARCHAR(15) DEFAULT 'undefined',  who_modified VARCHAR(15) DEFAULT 'undefined',  book_id char(36) default 'undefined' references APP.Book(id), time_zone VARCHAR(30));
create table app.ancient_scroll (id CHAR(36) DEFAULT 'undefined' PRIMARY KEY NOT NULL, length_of_scroll INT DEFAULT 0,  history_version INT DEFAULT 0);
create table app.long_web_page (id CHAR(7) DEFAULT 'undefined' PRIMARY KEY NOT NULL, lines_per_scroll_pane INT DEFAULT 0,  history_version INT DEFAULT 0);
create table app.very_long_web_page (id CHAR(4) DEFAULT 'undefined' PRIMARY KEY NOT NULL, inches_per_scroll_pane INT DEFAULT 0,  history_version INT DEFAULT 0);
create table encyclopedia_volume (     name               char(36) default 'undefined' not null, letter_of_alphabet char(36) default 'undefined' not null, primary key (name, letter_of_alphabet) );


