

-- a stored procedure to help us drop tables in Apache DB
-- since we have no 'if exists' language construct
create procedure DROP_TABLE ( schemaName varchar( 128 ), tableName varchar( 128 ) ) parameter style java modifies sql data language java external name 'persistence.DropTableUtility.dropTable' ;

call DROP_TABLE( 'app' , 'ancient_scroll' );
call DROP_TABLE( 'app' , 'long_web_page' );
call DROP_TABLE( 'app' , 'very_long_web_page' );
call DROP_TABLE( 'app', 'chapter' );
call DROP_TABLE( 'app', 'book' );
call DROP_TABLE( 'app', 'encyclopedia_volume' );



